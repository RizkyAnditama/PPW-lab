from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Muhammad Rizky Anditama' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998,7,17) #TODO Implement this, format (Year, Month, Date)
npm = 1706044080 # TODO Implement this
university = 'Universitas Indonesia'
hobby = 'Watch movies'
bio = "Hello, my name is Andi, i'm an university of indonesia student currently majoring in information system, and this is a part of my task"

friend1_name = "Ihsan Muhammad Aulia"
friend1_birth_date = date(1999,11,27)
friend1_npm = 1706043393
friend1_university = university
friend1_hobby = "Reading"
friend1_bio = "I love eating and sleeping"

friend2_name = "Sage Muhammad Abdullah"
friend2_birth_date = date(1999,6,9)
friend2_npm = 1706979455
friend2_university = university
friend2_hobby = "Do stuffs that matter"
friend2_bio = "I'm a computer science student, and I love cats"


# Create your views here. 
def index(request):  
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'university': university, 'hobby':hobby, 'bio':bio,
				 'friend1_name' : friend1_name, 'friend1_age' : calculate_age(friend1_birth_date.year), 'friend1_npm' : friend1_npm, 'friend1_university' : friend1_university, 'friend1_hobby' : friend1_hobby, 'friend1_bio' : friend1_bio,
				 'friend2_name' : friend2_name, 'friend2_age' : calculate_age(friend2_birth_date.year), 'friend2_npm' : friend2_npm, 'friend2_university' : friend2_university, 'friend2_hobby' : friend2_hobby, 'friend2_bio' : friend2_bio}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
